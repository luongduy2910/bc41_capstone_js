// TODO: Lấy dữ liệu (GET) trong mockAPI , render ra ngoài giao diện 
var productList = [] ;
var BASE_URL = "https://63bea7fae348cb0762149a43.mockapi.io"
function fetchDSSP() {
    batLoading() ; 
    axios({
        url : `${BASE_URL}/QLSP` , 
        method : 'GET',
    })
    .then(function(res){
        productList = res.data ; 
        renderSP(productList) ; 
        localStorage.setItem('QLSP', JSON.stringify(productList)) ; 
        tatLoading() ; 
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    })
}
// * render dữ liệu ra ngoài giao diện khi người dùng tải lại trang 

fetchDSSP() ; 

// TODO: Xây dựng tính năng cho phép người dùng filter sản phẩm theo loại 

function timKiemSP() {
    batLoading() ; 
    var loaiDTEl = document.getElementById('loaiDT').value ; 
    axios({
        url : `${BASE_URL}/QLSP` , 
        method : 'GET' , 
    })
    .then(function(res){
        if (loaiDTEl == "Tìm kiếm sản phẩm") {
            renderSP(res.data) ; 
            tatLoading(); 
        }
        else {
            var sanPhamMuonLoc = [] ; 
            for (let index = 0; index < res.data.length; index++) {
                const element = res.data[index];
                if (element.type == loaiDTEl) {
                    sanPhamMuonLoc.push(element) ; 
                }
            }
            renderSP(sanPhamMuonLoc) ; 
            tatLoading(); 
        }
    })
    .catch(function(err) {
        tatLoading() ; 
        console.log(err);
    })
}

productList = JSON.parse(localStorage.getItem('QLSP')) ; 

// TODO: Xây dựng tính năng thêm sản phẩm vào giỏ hàng 

var cartProduct = [] ; 
function themVaoGioHang(id) {
    var viTri = timKiemViTri(id , productList) ; 
    var sanphamduocchon = productList[viTri] ; 
    var sp = new CartItem(sanphamduocchon.id , sanphamduocchon.name , sanphamduocchon.price , 1) ; 
    if (cartProduct.length == 0) {
        cartProduct.push(sp) ;
    }else {
        var viTri = cartProduct.findIndex(function(item){
            return item.id == sanphamduocchon.id ; 
        })
        if (viTri != -1) {
             cartProduct[viTri].quantity++ ; 
    
        }else {
            cartProduct.push(sp) ; 

        }
    }   
    document.getElementById('thongbaosoluong').innerHTML = `${cartProduct.length}` ;  
    localStorage.setItem('GioHang' , JSON.stringify(cartProduct)) ;
    Toastify({

        text: "Thêm vào giỏ hàng thành công",
        
        duration: 1000
        
        }).showToast(); 

}



