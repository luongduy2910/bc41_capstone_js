// TODO: Hàm dùng để render dữ liệu trong mockAPI ra ngoài giao diện 
function renderSP(arrSP) {
    var contentHTML = "" ; 
    for (let index = 0; index < arrSP.length; index++) {
        const element = arrSP[index];
        var contentTr =
        `
        <div class = "col-lg-3 col-md-6 mb-3" >
            <div class="card p-3">
                <img class="card-img-top" src="${element.img}" alt="#">
                <div class="card-body">
                    <h4 class="card-title">${element.name}</h4>
                    <p class="card-text">${element.desc}</p>
                    <div class = "thongtin d-flex justify-content-between align-items-center" >
                    <span class = "card-price">${element.price}</span>
                    <button onclick = "themVaoGioHang(${element.id})" class = "btn btn-primary" >Add to Cart</button>
                    </div>
                </div>
            </div>
        </div>
        `
        contentHTML += contentTr ; 
    }
    document.getElementById('danhsachsanpham').innerHTML = contentHTML ; 
}

// TODO: Lớp đối tượng sản phẩm trong giỏ hàng


function CartItem(id , name , price , quantity) {
    this.id = id ; 
    this.name = name ; 
    this.price = price ; 
    this.quantity = quantity ;
}

// TODO: Tìm kiếm vị trí index sản phẩm khi biết trước id và array 

function timKiemViTri(id,arr) {
    var viTri = arr.findIndex(function(item){
        return item.id == id ; 
    })
    return viTri ; 
}