// TODO: Lấy giỏ hàng lên từ localStorage , chuyển từ dạng json -> mảng bằng parse 
var cartProduct = JSON.parse(localStorage.getItem('GioHang')) ; 

// TODO: Render giỏ hàng và tổng số tiền ra ngoài giao diện khi người dùng load lại trang 
renderGioHang(cartProduct) ; 
tongTien(cartProduct) ; 

// TODO: Xây dựng tính năng tăng số lượng , sau đó render lại ra ngoài giao diện (lúc này số lượng đã được cập nhật lại)
function tangSL(id) {
    var cartProduct = JSON.parse(localStorage.getItem('GioHang')) ; 
    var viTri = timKiemViTri(id , cartProduct) ; 
    console.log("🚀 ~ file: cart_main.js:14 ~ viTri ~ viTri", viTri)
    var element = cartProduct[viTri] ; 
    element.quantity++ ; 
    // * Lưu xuống localstorage sau khi đã tăng quantity 
    localStorage.setItem('GioHang', JSON.stringify(cartProduct)) ; 
    cartProduct = JSON.parse(localStorage.getItem('GioHang')) ; 
    //   * Render lại giao diện khi người dùng tăng số lượng 
    renderGioHang(cartProduct) ; 
    tongTien(cartProduct) ; 
}

// TODO: Xây dựng tính năng giảm số lượng , sau đó render lại ra ngoài giao diện (lúc này số lượng đã được cập nhật lại)
function giamSL(id) {
    var cartProduct = JSON.parse(localStorage.getItem('GioHang')) ; 
    var viTri = timKiemViTri(id , cartProduct) ; 
    console.log("🚀 ~ file: cart_main.js:31 ~ viTri ~ viTri", viTri)
    var element = cartProduct[viTri] ; 
    element.quantity--;  
    // * Lưu xuống localstorage sau khi đã tăng quantity 
    localStorage.setItem('GioHang', JSON.stringify(cartProduct)) ; 
    cartProduct = JSON.parse(localStorage.getItem('GioHang')) ; 
    //   * Render lại giao diện khi người dùng tăng số lượng 
    renderGioHang(cartProduct) ;
    tongTien(cartProduct) ;
}

// TODO: Xây dựng tính năng xóa sản phẩm , sau đó render lại giỏ hàng và tổng số tiền ra ngoài giao diện 

function xoaSP(id) {
    var cartProduct = JSON.parse(localStorage.getItem('GioHang')) ; 
    var viTri = timKiemViTri(id , cartProduct) ; 
    console.log("🚀 ~ file: cart_main.js:49 ~ viTri ~ viTri", viTri)
    cartProduct.splice(viTri , 1) ; 
    // * Lưu xuống localstorage sau khi đã tăng quantity 
    localStorage.setItem('GioHang', JSON.stringify(cartProduct)) ; 
    cartProduct = JSON.parse(localStorage.getItem('GioHang')) ; 
    //   * Render lại giao diện khi người dùng tăng số lượng 
    renderGioHang(cartProduct) ; 
    tongTien(cartProduct) ;

}

function thanhToan() {
    localStorage.removeItem('GioHang') ; 
    Toastify({

        text: "Thanh toán thành công",
        
        duration: 1000
        
        }).showToast();
    var arrCart = localStorage.getItem('GioHang') ? JSON.parse(localStorage.getItem('GioHang')) : [] ; 
    renderGioHang(arrCart) ;     
    document.getElementById('tongsotien').innerHTML = 0 ; 
}