// TODO: Xây dựng tính năng in sản phẩm trong giỏ hàng (ở đây là cartProduct) ra giao diện màn hình
function renderGioHang(arr){
    var contentHTML = "" ; 
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        var contentTr = 
        `
        <tr>
            <td>${element.name}</td>
            <td>${element.price * element.quantity}</td>
            <td>
                <div class="buttons_added">
                    <button style = "display: inline-block;
                    width: 30px;
                    height: 30px;
                    border: 1px solid white;"
                    onclick = "tangSL(${element.id})">+</button>
                    <span style = "display : inline-block ; margin : 0 10px">${element.quantity}</span>
                    <button style = "display: inline-block;
                    width: 30px;
                    height: 30px;
                    border: 1px solid white;"
                    onclick = "giamSL(${element.id})">-</button>
                </div>
            </td>
            <td><button onclick = "xoaSP(${element.id})" class = "btn btn-danger">Xóa</button></td>
        </tr>
        `
        contentHTML += contentTr ; 
    }
    document.getElementById('danhsachgiohang').innerHTML = contentHTML ; 
}

function tongTien(arr) { 
    var tong = 0 ; 
    arr.forEach(element => {
        var soTien = element.price * element.quantity ;
        tong += soTien ; 
    });
    document.getElementById('tongsotien').innerHTML = tong ; 
}


function timKiemViTri(id , arr){
    var viTri = arr.findIndex(function(item){
        return item.id == id ; 
    })
    return viTri ; 
}