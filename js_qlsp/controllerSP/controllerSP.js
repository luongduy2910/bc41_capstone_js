// TODO: Xây dựng chức năng render dữ liệu (arr) ra ngoài giao diện

function renderQLSP(arr) {
    var contentHTML = "" ; 
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        var contentTr = 
        `
        <tr>
            <td>${element.id}</td>
            <td>${element.name}</td>
            <td>${element.price}</td>
            <td>${element.img}</td>
            <td>${element.desc}</td>
            <td>
                <button data-toggle="modal" data-target="#myModal" onclick = "suaSP(${element.id})" class = "btn btn-primary mb-2" >Sửa</button>
                <button onclick = "xoaSP(${element.id})" class = "btn btn-danger" >Xóa</button>
            </td>
        </tr>
        `
        contentHTML += contentTr ; 
    }
    document.getElementById('tblDanhSachSanPham').innerHTML = contentHTML ; 
}

// TODO: Xây dựng chức năng lấy thông tin từ các ô input

function layThongTinTuForm() {
    var name = document.getElementById('SanPham').value ; 
    var price = document.getElementById('Gia').value ; 
    var screen = document.getElementById('ManHinh').value ; 
    var backCamera = document.getElementById('CameraSau').value ; 
    var frontCamera = document.getElementById('CameraTruoc').value ; 
    var img = document.getElementById('HinhAnh').value ; 
    var desc = document.getElementById('MoTa').value ; 
    var type = document.getElementById('LoaiDienThoai').value ; 
    var sp = new SanPham(name , price , screen , backCamera , frontCamera ,img , desc , type) ; 
    return sp ;
}

// TODO: Xây dựng chức năng làm mới form sau khi thêm mới và cập nhật 

function lamMoiForm() {
    document.getElementById('SanPham').value = "" ; 
    document.getElementById('Gia').value = "" ; 
    document.getElementById('ManHinh').value = "" ; 
    document.getElementById('CameraSau').value = "" ; 
    document.getElementById('CameraTruoc').value = "" ; 
    document.getElementById('HinhAnh').value = "" ; 
    document.getElementById('MoTa').value = "" ; 
    document.getElementById('LoaiDienThoai').value = "" ; 
}


// TODO: xây dựng chức năng show thông tin sản phẩm lên form để người dùng chỉnh sửa lại thông tin 

function showThongTinLenForm(arr) {
    document.getElementById('SanPham').disabled = true;
    document.getElementById('SanPham').value = arr.name ;  
    document.getElementById('Gia').value = arr.price ;  
    document.getElementById('ManHinh').value = arr.screen ;  
    document.getElementById('CameraSau').value = arr.backCamera ;  
    document.getElementById('CameraTruoc').value = arr.frontCamera ;  
    document.getElementById('HinhAnh').value = arr.img ;  
    document.getElementById('MoTa').value = arr.desc ;  
    document.getElementById('LoaiDienThoai').value = arr.type ;  
    document.getElementById('identify').value = arr.id ; 
}

function onSuccess() {
    Toastify({
    text: "Thêm sản phẩm thành công",
    duration: 3000
    }).showToast();
}

function onSuccess2() {
    Toastify({
    text: "Cập nhật sản phẩm thành công",
    duration: 3000
    }).showToast();
}

function onFailure() {
    Toastify({
    text: "Thêm sản phẩm thất bại",
    duration: 3000
    }).showToast();
}

function onFailure2() {
    Toastify({
    text: "Cập nhật sản phẩm thất bại",
    duration: 3000
    }).showToast();
}