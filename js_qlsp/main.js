// TODO: xây dựng chức năng fetchAPI , render toàn bộ dữ liệu trong mockAPI ra ngoài giao diện 


var BASE_URL = "https://63bea7fae348cb0762149a43.mockapi.io"
function fetchQLSP() {
    batLoading() ; 
    axios({
        url : `${BASE_URL}/QLSP` , 
        method : "GET",
    })
    .then(function(res){
        // * render dữ liệu ra ngoài giao diện
        renderQLSP(res.data) ; 
        tatLoading() ; 
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    })
} ; 

// * render dữ liệu khi người dùng reload lại trang 
fetchQLSP() ; 

// TODO: xây dựng chức năng xóa sản phẩm 

function xoaSP(id) {
    batLoading() ; 
    axios({
        url : `${BASE_URL}/QLSP/${id}` ,
        method : "DELETE" ,
    })
    .then(function(res){
        tatLoading() ; 
        // * render toàn bộ dữ liệu sau khi đã xóa dữ liệu thành công
        fetchQLSP() ; 
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    })
}

// TODO: xây dựng chức năng thêm sản phẩm 

function btnthemSP() {
    batLoading() ; 
    axios({
        url : `${BASE_URL}/QLSP` , 
        method : "GET" , 
    })
    .then(function(res){
        var isValid = kiemTraTrung(res.data) && kiemTraSanPham() ; 
        isValid = isValid 
        & kiemTraGia() 
        & kiemTraHinhAnh()
        & kiemTraMoTa() 
        & kiemTraManHinh()
        & kiemTraCameraTruoc() 
        & kiemTraCameraSau() 
        & kiemTraLoaiDT() ; 
        if (isValid) {
            axios({
                url : `${BASE_URL}/QLSP` , 
                method : "POST" , 
                data : layThongTinTuForm() , 
            })
            .then(function(res){
                tatLoading() ; 
                fetchQLSP() ; 
                lamMoiForm() ; 
                $('#myModal').modal('hide') ; 
                onSuccess() ; 
            })
            .catch(function(err){
                tatLoading() ; 
                onFailure() ; 
                console.log(err) ;
            })
        }else {
            tatLoading() ; 
            alert('Thêm sản phẩm thất bại') ; 
        }
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    })
}
// TODO: xây dựng chắc năng sửa thông tin sản phẩm

function suaSP(id) {
    batLoading() ; 
    axios({
        url : `${BASE_URL}/QLSP/${id}` , 
        method : "GET" , 
    })
    .then(function(res){
        tatLoading(); 
        showThongTinLenForm(res.data) ; 
        document.getElementById('themSP').style.display = "none" ; 
        document.getElementById('capNhatSP').style.display = "block" ; 
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    }) 
}

// TODO: sau khi người dùng chỉnh sửa lại thông tin => cập nhật lại thông tin và render ra ngoài giao diện 

function btncapNhatSP() {
    var id = document.getElementById('identify').value ; 
    var isValid = kiemTraSanPham() ; 
    isValid = isValid 
        & kiemTraGia() 
        & kiemTraHinhAnh()
        & kiemTraMoTa() 
        & kiemTraManHinh()
        & kiemTraCameraTruoc() 
        & kiemTraCameraSau() 
        & kiemTraLoaiDT() ; 
        if (isValid) {
            axios({
                url : `${BASE_URL}/QLSP/${id}` , 
                method : "PUT" , 
                data : layThongTinTuForm() , 
            })
            .then(function(res){
                tatLoading() ; 
                fetchQLSP() ; 
                document.getElementById('SanPham').disabled = false ; 
                document.getElementById('themSP').style.display = "block" ; 
                document.getElementById('capNhatSP').style.display = "none" ; 
                onSuccess2() ; 
                $('#myModal').modal('hide') ; 
                lamMoiForm() ; 
            })
            .catch(function(err){
                tatLoading() ; 
                console.log(err);
                onFailure2() ; 
            })
        }
}