// TODO: kiểm tra validate các ô input thông tin sản phẩm 

// * mã sản phẩm (sảm phẩm) không được để trống và trùng nhau 

function kiemTraTrung(arr) {
    var flag = true ; 
    var sp = document.getElementById('SanPham').value ; 
    var viTri = arr.findIndex(function(item){
        return item.name == sp ; 
    })
    if (viTri != -1) {
        document.getElementById('tb__sp').innerHTML = "Sản phẩm đã tồn tại"
        flag = false ; 
    }else {
        document.getElementById('tb__sp').innerHTML = "" ; 
        flag = true ; 
    }
    return flag ;
}

function kiemTraSanPham() {
    var flag = true ; 
    var sp = document.getElementById('SanPham').value ; 
    if (sp == "") {
        document.getElementById('tb__sp').innerHTML = "Sản phẩm không được bỏ trống" ; 
        flag = false ; 
    }else {
        document.getElementById('tb__sp').innerHTML = "" ; 
        flag = true ; 
    }
    return flag ;
}

// * Giá sản phẩm không được để trống và phải là số 

function kiemTraGia() {
    var flag = true ; 
    var gia = document.getElementById('Gia').value ; 
    var letters = /^\d+$/ ; 
    if (letters.test(gia)){
        document.getElementById('tb__gia').innerHTML = "" ; 
        flag = true ; 
    }else if (gia == "") {
        document.getElementById('tb__gia').innerHTML = "Giá không được bỏ trống" ; 
        flag = false ; 
    }else {
        document.getElementById('tb__gia').innerHTML = "Vui lòng nhập lại giá" ; 
        flag = false ; 
    }
    return flag ; 
}

// * đường link hình ảnh theo định dạng HTTP　URLs và không được để trống

function kiemTraHinhAnh() {
    var flag = true ; 
    var img = document.getElementById('HinhAnh').value ; 
    var letters = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', // fragment locator
  'i') ; 
  if (letters.test(img)){
    document.getElementById('tb__img').innerHTML = "" ; 
    flag = true ;
  }else if(img == ""){
    document.getElementById('tb__img').innerHTML = "Hình ảnh không được để trống" ;
    flag = false ; 
  }else {
    document.getElementById('tb__img').innerHTML = "Vui lòng nhập lại hình ảnh. hình ảnh theo dạng url ví dụ: https:/..../....." ; 
    flag = false ; 
  }
  return flag ;
}

// * Mô tả không được để trống và kí tự không quá 40 

function kiemTraMoTa(){
    var flag = true ; 
    var mota = document.getElementById('MoTa').value ; 
    if (mota.length > 0 && mota.length <=40) {
        document.getElementById('tb__mota').innerHTML = "" ; 
        flag = true ; 
    }else {
        document.getElementById('tb__mota').innerHTML = "Mô tả không vượt quá 40 kí tự" ; 
        flag = false ; 
    }
    return flag ;
}

// * Màn hình , camera trước sau không được để trống 

function kiemTraManHinh(){
    var flag = true ; 
    var manhinh = document.getElementById('ManHinh').value ; 
    if (manhinh == ""){
        document.getElementById('tb__manhinh').innerHTML = "Màn hình không được để trống" ; 
        flag = false ;
    }else {
        document.getElementById('tb__manhinh').innerHTML = "" ; 
        flag = true ;
    }
    return flag ;
}

function kiemTraCameraTruoc(){
    var flag = true ; 
    var camtruoc = document.getElementById('CameraTruoc').value ; 
    if (camtruoc == ""){
        document.getElementById('tb__camtruoc').innerHTML = "Camera trước không được để trống" ; 
        flag = false ;
    }else {
        document.getElementById('tb__camtruoc').innerHTML = "" ; 
        flag = true ;
    }
    return flag ;
}

function kiemTraCameraSau(){
    var flag = true ; 
    var camsau = document.getElementById('CameraSau').value ; 
    if (camsau == ""){
        document.getElementById('tb__camsau').innerHTML = "Camera sau không được để trống" ; 
        flag = false ;
    }else {
        document.getElementById('tb__camsau').innerHTML = "" ; 
        flag = true ;
    }
    return flag ;
}

// * Loại điện thoại phải chọn , không được bỏ trống 

function kiemTraLoaiDT() {
    var flag = true ; 
    var loai = document.getElementById('LoaiDienThoai').value ; 
    if (loai == "Chọn loại điện thoại" || loai == "") {
        document.getElementById('tb__loai').innerHTML = "Vui lòng chọn loại điện thoại" ; 
        flag = false ; 
    }else {
        document.getElementById('tb__loai').innerHTML = "" ; 
        flag = true ;
    }
    return flag ; 
}

